#!/bin/bash

# Settings
DATA=$(sed -n '3p' ./config.itk|cut -d'=' -f2-)

F=$(which find)
G=$(which gzip)

if [ -z "$1" ]; then
  echo -en "String to search? [ENTER]\t"
  read search
else
  search="${@}"
fi

echo -en "\nThe string ${search} found in:\n\n"
for t in $(${F} /home/itzhak/data/whois/ -type f); do
  if [[ "${t}" =~ \.gz$ ]]; then
    ${G} -cd ${t}|grep -i "${search}" >/dev/null 2>&1
    if [ "$?" == "0" ]; then
      echo ${t}
    fi
  fi
done

echo -en "\nTo view the content use 'gzip -cd [FILENAME]'\n\n"

