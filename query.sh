#!/bin/bash

# Locate the whois command
W=`which whois`
W="${W} -H"
G=`which gzip`
V=$(echo "$BASH_VERSION"|cut -d'.' -f 1)	# Checking bash version

# Data Dir
DATA=$(sed -n '3p' ./config.itk|cut -d'=' -f2-)
# Current Date
DATE=`date +"%Y%m%d-%H%M"`

if [ -z "${1}" ]; then
  echo "Usage: ${0} domain.tld";
  exit 3;
fi
DV=${2}

if [ "${V}" -lt "4" ]; then
  echo "Version 4 of bash is required"
  exit 4;
fi

# Function
unset -f viewhois;
viewhois() {
  if [ "${DV}" == "disable_view" ]; then
    exit 0;
  fi

  echo "[+] Viewing in 1 seconds ...";
  echo "";
  sleep 1;
  ${G} -cd ${DATA}/${RIGHT}/${LEFT}/${DATE}.gz
  exit 0;
}

if [ ! -d "${DATA}" ]; then
  echo "Error DATA dir doesn't exists";
  exit 2;
fi

# Perform a whois query
DOMAIN=$(echo ${1,,})
echo "[+] Query Sent ... ${DOMAIN}";
WHOIS=`${W} ${DOMAIN}|\
grep -iv -e \
"# Cached this answer " -e \
"# local time    :" -e \
"# gmt time      :" -e \
"# last modify   :" -e \
"# request from  :" -e \
"Last update" -e \
"Timestamp" -e \
"lookup made at" -e \
"look-up made at" -e \
" lookups left today" -e \
"Query Time: " -e \
"Query Source: " -e \
"WHOIS Source: " -e \
"WHOIS lookup made on "`

# Check the Exit Code of the whois command
WHOIS_EXIT_CODE="$?"

LEFT=`echo "${DOMAIN}"|cut -d'.' -f1`
RIGHT=`echo "${DOMAIN}"|cut -d'.' -f2-`

if [ ${WHOIS_EXIT_CODE} -ne "0" ]; then
  echo "Error ${WHOIS_EXIT_CODE}";
  exit ${WHOIS_EXIT_CODE};
fi

echo "[+] Answer Recieved";

if [ ! -d "${DATA}/${RIGHT}" ]; then
  mkdir ${DATA}/${RIGHT}
fi

if [ ! -d "${DATA}/${RIGHT}/${LEFT}" ]; then
  mkdir ${DATA}/${RIGHT}/${LEFT}
  echo "${WHOIS}" > ${DATA}/${RIGHT}/${LEFT}/${DATE}
  ${G} -9 ${DATA}/${RIGHT}/${LEFT}/${DATE}
  echo "[+] Saved ${DOMAIN}"
  viewhois;
  exit 0;
else
  LATEST=`ls -t ${DATA}/${RIGHT}/${LEFT}/*|head -1`
  WHOIS_OLD=`${G} -cd ${LATEST}`
  echo -n "[+] Found previuos answer ... ";
  if [ "${WHOIS}" = "${WHOIS_OLD}" ]; then
    echo "Same";
    if [ "${DV}" == "disable_view" ]; then
      echo "";
      exit 0;
    fi
    echo "[+] Viewing in 3 seconds ..."
    echo "";
    sleep 3;
    echo "${WHOIS_OLD}";
    exit 0;
  else
    echo "Not the same, updating";
    echo "${WHOIS}" > ${DATA}/${RIGHT}/${LEFT}/${DATE}
    ${G} -9 ${DATA}/${RIGHT}/${LEFT}/${DATE}
    viewhois;
  fi
fi
