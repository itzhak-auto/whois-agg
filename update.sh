#!/bin/bash

# Settings
DIR=$(sed -n '3p' ./config.itk|cut -d'=' -f2-)
SLEEP=$(sed -n '4p' ./config.itk|cut -d'=' -f2-)
WBIN="./query.sh"

if [ ! -f "${WBIN}" ]; then
  echo "[+] Cannot find '${WBIN}' script."
  exit 1;
fi

TLD=(`ls ${DIR}`)
CTLD=`echo "${TLD[@]}"|wc -w`
SIZE=`du -h ${DIR}|tail -n 1| cut -f1`

for t in "${TLD[@]}"; do
  DOMAIN=(`ls ${DIR}/${t}`)

  for d in "${DOMAIN[@]}"; do
    i=$((i+1))
    FQDN[${i}]=`echo ${d}.${t}`
  done
done

CDOMAIN=`echo "${FQDN[@]}"|wc -w`

unset i
echo ""
echo -e "\t TLD Count: ${CTLD}\t Domain Count: ${CDOMAIN}\t Size: ${SIZE}\t Sleep: ${SLEEP}s"
RT=`expr ${CDOMAIN} \* ${SLEEP} / 60 / 60`
echo -e "\t Estimated running time for full update: ${RT} Hours"

echo ""

if [ "$1" == "-count" ]; then
  exit 0;
fi

for f in "${FQDN[@]}"; do
  i=$((i+1))
  echo "(${i}/${CDOMAIN}) ${f}"
  $WBIN ${f} disable_view
  sleep ${SLEEP}
done

echo "";

exit 0;
