# What is "Whois Aggregator"

Whenever you use this script to perform a whois query on a domain
the output will be saved and sorted. If you query the same domain
again in a later time, the output will be checked against the the
last saved one to see if it's different, if it is, a new file
will be saved.

# Laws and permissions

Most if not all whois servers copyright their output, meaning you
**can't** reproduce their content beyond self use! You should
also avoid performing alot of queries in a short period of time
as you may get blocked for excess queries.

The './update.sh' script may be considered as an automated tool
which is against most whois maintainers rules, use on your own
risk, I will just tip you to use some kind of proxychains
solution.

# Configuring

Always 'cd' into the folder holding these files, don't run it from
a different PATH, as the 'config.itk' is checked relatively.

Modify 'config.itk', 'DIR' should be equal to the location where
you wish to save the data, and 'SLEEP' should be the time in
seconds to wait between queries in the './update.sh' script. don't
change the order of lines, 'DIR' should be on the 3rd line and
'SLEEP' need to be on the 4th.

Make sure you have an whois client installed
	
	dpkg -s whois

Create /etc/whois.conf copy the one supplied here, run as root

	cp ./whois.conf /etc/whois.conf

# How to use?

to query:

	./query.sh <DOMAIN NAME.TLD>

to update all records on database (**not recommended**):

	./update.sh

to count entries and other useful information (the update script
will not run in the end of counting):

	./update.sh -count

to search:

	./find.sh

and enter the string you wish to search when asked.


# Notice

Some whois server adds lines like "local time", "last modify",
"last update" and so on, but the content itself wasn't
changed, I added rules to drop the lines start with these
strings, see lines 48-61 in 'query.sh'.

Also, there **isn't** any sanity check on the string you request
to query, make sure you always enter a valid domain and not
random text.
